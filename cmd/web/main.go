package main

import (
	"fmt"
	"github.com/alexedwards/scs/v2"
	"gitlab.com/rakhimovsh/simple-golang/pkg/config"
	"gitlab.com/rakhimovsh/simple-golang/pkg/handlers"
	"gitlab.com/rakhimovsh/simple-golang/pkg/render"
	"log"
	"net/http"
	"time"
)

const PORT = ":8080"

var app config.AppConfig
var session *scs.SessionManager

func main() {

	app.InProduction = false

	session = scs.New()
	session.Lifetime = 24 * time.Hour
	session.Cookie.Persist = true
	session.Cookie.SameSite = http.SameSiteLaxMode
	session.Cookie.Secure = app.InProduction

	app.Session = session

	tc, err := render.CreateTemplateCache()
	if err != nil {
		log.Fatal(err)
	}
	repo := handlers.NewRepo(&app)
	handlers.NewHandlers(repo)
	render.NewTemplates(&app)

	app.TemplateCache = tc
	app.UseCache = false
	fmt.Println("Server started in ", PORT)
	srv := &http.Server{
		Addr:    PORT,
		Handler: routes(&app),
	}

	err = srv.ListenAndServe()
	log.Fatal(err)
}
